# README #

Translation of 

[John Hughes Why Functional Programming Matters. In D. Turner, editor, Research Topics in Functional Programming, Addison-Wesley, 1990.](https://www.cs.kent.ac.uk/people/staff/dat/miranda/whyfp90.pdf)

into Russian language [Почему функциональное программирование.](/src/Why%20functional%20programming.pdf)
